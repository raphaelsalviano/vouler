package br.com.ufpb.vouler.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.ufpb.vouler.R;
import br.com.ufpb.vouler.VouLerApplication;
import br.com.ufpb.vouler.activities.LevelActivity;
import br.com.ufpb.vouler.model.Exercice;
import br.com.ufpb.vouler.model.Exercice1;

public class FragmentExerc1 extends Fragment {

    private TextView mTitle;
    private View mButtonAudio;

    private View mButtonOption1;
    private ImageView mImageOption1;
    private TextView mTextOption1;

    private View mButtonOption2;
    private ImageView mImageOption2;
    private TextView mTextOption2;

    private View mButtonOption3;
    private ImageView mImageOption3;
    private TextView mTextOption3;

    private View mButtonOption4;
    private ImageView mImageOption4;
    private TextView mTextOption4;

    private Button mButtonConfirm;

    private VouLerApplication application;

    private boolean isContinue = false;
    private Exercice1 exercice1;
    private int lession;

    private String TAG = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        application = (VouLerApplication) getActivity().getApplicationContext();
        if(getArguments() != null){
            lession = getArguments().getInt("lession");
            long id = getArguments().getLong("id");
            TAG = getArguments().getString("TAG");
            exercice1 = (Exercice1) application.getExerciceTo(lession, id);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_exerc1, container, false);

        findViewById(view);
        loadingQuestion(exercice1);
        String[] selecion = onCLick();
        onCLickButtonSend(selecion);

        return view;
    }

    private void findViewById(View view){
        mTitle = (TextView) view.findViewById(R.id.title_exerc1);
        mButtonAudio = view.findViewById(R.id.audio_exerc1);

        mButtonOption1 = view.findViewById(R.id.item_level_list_1);
        mImageOption1 = (ImageView) view.findViewById(R.id.option_img1);
        mTextOption1 = (TextView) view.findViewById(R.id.option_text1);

        mButtonOption2 = view.findViewById(R.id.item_level_list_2);
        mImageOption2 = (ImageView) view.findViewById(R.id.option_img2);
        mTextOption2 = (TextView) view.findViewById(R.id.option_text2);

        mButtonOption3 = view.findViewById(R.id.item_level_list_3);
        mImageOption3 = (ImageView) view.findViewById(R.id.option_img3);
        mTextOption3 = (TextView) view.findViewById(R.id.option_text3);

        mButtonOption4 = view.findViewById(R.id.item_level_list_4);
        mImageOption4 = (ImageView) view.findViewById(R.id.option_img4);
        mTextOption4 = (TextView) view.findViewById(R.id.option_text4);

        mButtonConfirm = (Button) view.findViewById(R.id.button_send_confirmed);
    }

    private void loadingQuestion(final Exercice1 exercice){
        mTitle.setText(exercice.getTitle());
        mButtonAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                application.voiceText(exercice.getTitle());
            }
        });

        mImageOption1.setImageDrawable(getResources().getDrawable(exercice.getDrawableImages().get(0)));
        mTextOption1.setText(exercice.getTitleDrawables().get(0));

        mImageOption2.setImageDrawable(getResources().getDrawable(exercice.getDrawableImages().get(1)));
        mTextOption2.setText(exercice.getTitleDrawables().get(1));

        mImageOption3.setImageDrawable(getResources().getDrawable(exercice.getDrawableImages().get(2)));
        mTextOption3.setText(exercice.getTitleDrawables().get(2));

        mImageOption4.setImageDrawable(getResources().getDrawable(exercice.getDrawableImages().get(3)));
        mTextOption4.setText(exercice.getTitleDrawables().get(3));
    }

    private String[] onCLick(){
        final String[] selection = {""};

        mButtonOption1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mButtonOption1.setBackgroundResource(R.drawable.btn_dark_boder);
                mButtonOption2.setBackgroundResource(R.drawable.raised_button_border);
                mButtonOption3.setBackgroundResource(R.drawable.raised_button_border);
                mButtonOption4.setBackgroundResource(R.drawable.raised_button_border);
                mButtonConfirm.setEnabled(true);
                mButtonConfirm.setBackgroundResource(R.drawable.raised_button_blue);
                selection[0] = mTextOption1.getText().toString();
            }
        });

        mButtonOption2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mButtonOption2.setBackgroundResource(R.drawable.btn_dark_boder);
                mButtonOption1.setBackgroundResource(R.drawable.raised_button_border);
                mButtonOption3.setBackgroundResource(R.drawable.raised_button_border);
                mButtonOption4.setBackgroundResource(R.drawable.raised_button_border);
                mButtonConfirm.setEnabled(true);
                mButtonConfirm.setBackgroundResource(R.drawable.raised_button_blue);
                selection[0] = mTextOption2.getText().toString();
            }
        });

        mButtonOption3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mButtonOption3.setBackgroundResource(R.drawable.btn_dark_boder);
                mButtonOption1.setBackgroundResource(R.drawable.raised_button_border);
                mButtonOption2.setBackgroundResource(R.drawable.raised_button_border);
                mButtonOption4.setBackgroundResource(R.drawable.raised_button_border);
                mButtonConfirm.setEnabled(true);
                mButtonConfirm.setBackgroundResource(R.drawable.raised_button_blue);
                selection[0] = mTextOption3.getText().toString();
            }
        });

        mButtonOption4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mButtonOption4.setBackgroundResource(R.drawable.btn_dark_boder);
                mButtonOption1.setBackgroundResource(R.drawable.raised_button_border);
                mButtonOption2.setBackgroundResource(R.drawable.raised_button_border);
                mButtonOption3.setBackgroundResource(R.drawable.raised_button_border);
                mButtonConfirm.setEnabled(true);
                mButtonConfirm.setBackgroundResource(R.drawable.raised_button_blue);
                selection[0] = mTextOption4.getText().toString();
            }
        });

        return selection;
    }

    private void onCLickButtonSend(final String selecion[]){
        mButtonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isContinue){
                    if(selecion[0].equalsIgnoreCase(exercice1.getResponse())){
                        Snackbar.make(view, "Correto!", Snackbar.LENGTH_SHORT).show();
                    }else{
                        Snackbar.make(view, "Errado!", Snackbar.LENGTH_SHORT).show();
                    }
                    mButtonConfirm.setText("Continuar");
                    mButtonConfirm.setAllCaps(true);
                    isContinue = true;
                }else{
                    // Muda pro proximo exercicio
                    application.decrementarLession();
                    trocarTela();

                }
            }
        });
    }

    private void trocarTela(){
        Fragment fragment = null;
        Exercice exercice = application.getExerciceRandom(lession);
        if(exercice.getTypeExercice() == 1){
            fragment = new FragmentExerc1();
        }else if(exercice.getTypeExercice() == 2){
            fragment = new FragmentExerc2();
        }else if(exercice.getTypeExercice() == 3){
            fragment = new FragmentExerc3();
        }else{
            fragment = new FragmentExerc4();
        }

        Bundle bundle = new Bundle();
        bundle.putInt("lession", lession);
        bundle.putLong("id", exercice.get_id());
        fragment.setArguments(bundle);

        if(application.getQuantLessions() > 0){
            application.decrementarLession();
            getFragmentManager().beginTransaction().replace(R.id.container_exercices, fragment).commit();
            onDestroy();
        }else{
            Intent intent = new Intent(getActivity(), LevelActivity.class);
            intent.putExtra("TAG", TAG);
            startActivity(intent);
            getActivity().finish();
        }
    }

}
