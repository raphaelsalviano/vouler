package br.com.ufpb.vouler.model;

import java.io.Serializable;
import java.util.List;

public class Lession implements Serializable {

    private int _id;
    private String description;
    private List<Exercice> exercices;

    public Lession() {
    }

    public Lession(int _id, String description, List<Exercice> exercices) {
        this._id = _id;
        this.description = description;
        this.exercices = exercices;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Exercice> getExercices() {
        return exercices;
    }

    public void setExercices(List<Exercice> exercices) {
        this.exercices = exercices;
    }
}
