package br.com.ufpb.vouler.model;

import java.util.List;

/**
 * Created by rapha on 07/06/2016.
 */
public class Exercice1 implements Exercice {

    private long _id;
    private int typeExercice;
    private String title;
    private List<Integer> drawableImages;
    private List<String> titleDrawables;
    private String response;

    public Exercice1(long _id, int typeExercice, String title, List<Integer> drawableImages, List<String> titleDrawables, String response) {
        this._id = _id;
        this.typeExercice = typeExercice;
        this.title = title;
        this.drawableImages = drawableImages;
        this.titleDrawables = titleDrawables;
        this.response = response;
    }

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public int getTypeExercice() {
        return typeExercice;
    }

    public void setTypeExercice(int typeExercice) {
        this.typeExercice = typeExercice;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Integer> getDrawableImages() {
        return drawableImages;
    }

    public void setDrawableImages(List<Integer> drawableImages) {
        this.drawableImages = drawableImages;
    }

    public List<String> getTitleDrawables() {
        return titleDrawables;
    }

    public void setTitleDrawables(List<String> titleDrawables) {
        this.titleDrawables = titleDrawables;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
