package br.com.ufpb.prolicen.vouler.util.fonts;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by rapha on 13/04/2016.
 */
public class RobotoMedium extends TextView {

    public RobotoMedium(Context context) {
        super(context);
        createTypeface(context);
    }

    public RobotoMedium(Context context, AttributeSet attrs) {
        super(context, attrs);
        createTypeface(context);
    }

    public RobotoMedium(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createTypeface(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public RobotoMedium(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createTypeface(context);
    }

    private void createTypeface(Context context){
        setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/roboto_medium.ttf"));
    }
}
