package br.com.ufpb.vouler.model;

import java.io.Serializable;
import java.util.List;

public class Level implements Serializable {

    private int _id;
    private String TAG;
    private String nameLevel;
    private int level;
    private Lession[] lessions;

    public Level() {
        this.lessions = new Lession[2];
    }

    public Level(int _id, String TAG, String nameLevel, int level, Lession[] lessions) {
        this._id = _id;
        this.TAG = TAG;
        this.nameLevel = nameLevel;
        this.level = level;
        this.lessions = lessions;
    }

    public String getNameLevel() {
        return nameLevel;
    }

    public void setNameLevel(String nameLevel) {
        this.nameLevel = nameLevel;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public Lession[] getLessions() {
        return lessions;
    }

    public void setLessions(Lession[] lessions) {
        this.lessions = lessions;
    }

    public String getTAG() {
        return TAG;
    }

    public void setTAG(String TAG) {
        this.TAG = TAG;
    }
}
