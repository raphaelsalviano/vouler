package br.com.ufpb.vouler.fragment;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.ufpb.vouler.R;
import br.com.ufpb.vouler.VouLerApplication;
import br.com.ufpb.vouler.activities.LevelActivity;
import br.com.ufpb.vouler.model.Exercice;
import br.com.ufpb.vouler.model.Exercice3;

public class FragmentExerc3 extends Fragment {

    private VouLerApplication application;

    private boolean isContinue = false;

    private TextView mTitle;
    private View mButtonAudio;

    private ImageView imageView;
    private EditText editText;

    private Button mButtonConfirm;

    private Exercice3 exercice3;
    private int lession;

    private String TAG = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        application = (VouLerApplication) getActivity().getApplicationContext();
        if(getArguments() != null){
            lession = getArguments().getInt("lession");
            long id = getArguments().getLong("id");
            TAG = getArguments().getString("TAG");
            exercice3 = (Exercice3) application.getExerciceTo(lession, id);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_exerc3, container, false);

        findViewById(view);
        loadingQuestion(exercice3);
        onCLick();
        onCLickButtonSend();

        return view;
    }

    private void findViewById(View view){
        mTitle = (TextView) view.findViewById(R.id.title_exerc3);
        mButtonAudio = view.findViewById(R.id.audio_exerc3);

        //((TextInputLayout) view.findViewById(R.id.editTextLayout)).setTypeface(getTypeface());

        imageView = (ImageView) view.findViewById(R.id.img_exerc3);
        editText = (EditText) view.findViewById(R.id.entrada_exerc3);

        mButtonConfirm = (Button) view.findViewById(R.id.button_send_confirmed3);
    }

    private void loadingQuestion(final Exercice3 exercice){
        mTitle.setText(exercice.getTitle());
        mButtonAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                application.voiceText(exercice.getTitle());
            }
        });

        imageView.setImageResource(exercice.getIdDrawable());
        imageView.setClickable(true);

        editText.setTypeface(getTypeface());
    }

    private void onCLick(){

        mButtonAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                application.voiceText(exercice3.getTitle());
            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                application.voiceText(exercice3.getResponse());
            }
        });

        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if(i == R.id.login || i == EditorInfo.IME_NULL){
                    mButtonConfirm.setEnabled(true);
                    mButtonConfirm.setBackgroundResource(R.drawable.raised_button_blue);
                    return true;
                }
                return false;
            }
        });

    }

    private void onCLickButtonSend(){
        final String[] selecion = new String[]{editText.getText().toString()};
        mButtonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isContinue){
                    if(selecion[0].equalsIgnoreCase(exercice3.getResponse())){
                        Snackbar.make(view, "Correto!", Snackbar.LENGTH_SHORT).show();
                    }else{
                        Snackbar.make(view, "Errado!", Snackbar.LENGTH_SHORT).show();
                    }
                    mButtonConfirm.setText("Continuar");
                    mButtonConfirm.setAllCaps(true);
                    isContinue = true;
                }else{
                    // Muda pro proximo exercicio
                    application.decrementarLession();
                    trocarTela();
                }
            }
        });
    }

    private void trocarTela(){
        Fragment fragment = null;
        Exercice exercice = application.getExerciceRandom(lession);
        if(exercice.getTypeExercice() == 1){
            fragment = new FragmentExerc1();
        }else if(exercice.getTypeExercice() == 2){
            fragment = new FragmentExerc2();
        }else if(exercice.getTypeExercice() == 3){
            fragment = new FragmentExerc3();
        }else{
            fragment = new FragmentExerc4();
        }

        Bundle bundle = new Bundle();
        bundle.putInt("lession", lession);
        bundle.putLong("id", exercice.get_id());
        fragment.setArguments(bundle);

        if(application.getQuantLessions() > 0){
            getFragmentManager().beginTransaction().replace(R.id.container_exercices, fragment).commit();
            onDestroy();
        }else{
            Intent intent = new Intent(getActivity(), LevelActivity.class);
            intent.putExtra("TAG", TAG);
            startActivity(intent);
            getActivity().finish();
        }
    }

    private Typeface getTypeface(){
        return Typeface.createFromAsset(getActivity().getAssets(), "fonts/roboto_regular.ttf");
    }

}
