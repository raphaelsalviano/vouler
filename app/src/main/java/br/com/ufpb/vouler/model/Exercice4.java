package br.com.ufpb.vouler.model;

/**
 * Created by rapha on 07/06/2016.
 */
public class Exercice4 implements Exercice {

    private long _id;
    private int typeExercice;
    private String title;
    private int idDrawable;
    private String response;

    public Exercice4(long _id, int typeExercice, String title, int idDrawable, String response) {
        this._id = _id;
        this.typeExercice = typeExercice;
        this.title = title;
        this.idDrawable = idDrawable;
        this.response = response;
    }

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public int getTypeExercice() {
        return typeExercice;
    }

    public void setTypeExercice(int typeExercice) {
        this.typeExercice = typeExercice;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIdDrawable() {
        return idDrawable;
    }

    public void setIdDrawable(int idDrawable) {
        this.idDrawable = idDrawable;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
