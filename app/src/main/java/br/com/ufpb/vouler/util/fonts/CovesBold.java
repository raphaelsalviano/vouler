package br.com.ufpb.prolicen.vouler.util.fonts;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by rapha on 13/04/2016.
 */
public class CovesBold extends TextView {

    public CovesBold(Context context) {
        super(context);
        createTypeface(context);
    }

    public CovesBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        createTypeface(context);
    }

    public CovesBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createTypeface(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CovesBold(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createTypeface(context);
    }

    private void createTypeface(Context context){
        setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/coves_bold.otf"));
    }
}
