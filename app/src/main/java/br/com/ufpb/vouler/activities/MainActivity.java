package br.com.ufpb.vouler.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import br.com.ufpb.vouler.R;
import br.com.ufpb.vouler.VouLerApplication;

public class MainActivity extends AppCompatActivity {

    private View mBasic01;
    private View mBasic02;
    private View mPractice01;
    private View mConsonants;

    private VouLerApplication application;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        application = (VouLerApplication) getApplicationContext();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_main);
        setSupportActionBar(toolbar);

        mBasic01 = findViewById(R.id.main_basic1);
        mBasic01.setOnClickListener(new OnClickMenuLevel("basic01"));

        mBasic02 = findViewById(R.id.main_basic2);
        mBasic02.setOnClickListener(new OnClickMenuLevel("basic02"));

        mPractice01 = findViewById(R.id.main_practice_unit_1);
        mPractice01.setOnClickListener(new OnClickMenuLevel("pratice01"));

        mConsonants = findViewById(R.id.main_words1);
        mConsonants.setOnClickListener(new OnClickMenuLevel("consonants1"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if(id == R.id.points_action){
            Toast.makeText(MainActivity.this, "Sua pontuação é de " + application.getPontuacao() + " pontos", Toast.LENGTH_SHORT).show();
            return true;
        }else if(id == R.id.profile_action){
            Toast.makeText(MainActivity.this, "Em breve", Toast.LENGTH_SHORT).show();
            return true;
        }else if(id == R.id.settings_action){
            Toast.makeText(MainActivity.this, "Em breve", Toast.LENGTH_SHORT).show();
            return true;
        }else if(id == R.id.exit_action){
            Toast.makeText(MainActivity.this, "Em breve", Toast.LENGTH_SHORT).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class OnClickMenuLevel implements View.OnClickListener{

        private final String TAG;

        private OnClickMenuLevel(String tag) {
            TAG = tag;
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(MainActivity.this, LevelActivity.class);
            intent.putExtra("TAG", TAG);
            startActivity(intent);
            finish();
        }
    }
}
