package br.com.ufpb.vouler.util.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;
import java.util.logging.Level;

import br.com.ufpb.vouler.R;

public class LevelAdapter extends RecyclerView.Adapter<LevelAdapter.LevelViewHolder> {

    private Context context;
    private List<Level> levels;

    public LevelAdapter(Context context) {
        this.context = context;
    }

    @Override
    public LevelViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_content_level, parent, false);
        return new LevelViewHolder(view);
    }

    @Override
    public void onBindViewHolder(LevelViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return levels.size();
    }

    public class LevelViewHolder extends RecyclerView.ViewHolder {

        private View mItemClick;
        private TextView mTextSizeActivities;
        private TextView mTextDescriptionLevel;
        private Button mButtonStart;

        public LevelViewHolder(View view) {
            super(view);

            mItemClick = view.findViewById(R.id.item_level_list);
            mTextSizeActivities = (TextView) view.findViewById(R.id.text_activities);
            mTextDescriptionLevel = (TextView) view.findViewById(R.id.text_description);
            mButtonStart = (Button) view.findViewById(R.id.button_start_level);
        }
    }
}
