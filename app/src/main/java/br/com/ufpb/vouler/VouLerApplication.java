package br.com.ufpb.vouler;

import android.app.Application;
import android.graphics.drawable.RippleDrawable;
import android.speech.tts.TextToSpeech;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import br.com.ufpb.vouler.model.Exercice;
import br.com.ufpb.vouler.model.Exercice1;
import br.com.ufpb.vouler.model.Exercice2;
import br.com.ufpb.vouler.model.Exercice3;
import br.com.ufpb.vouler.model.Exercice4;
import br.com.ufpb.vouler.model.Lession;
import br.com.ufpb.vouler.model.Level;
import br.com.ufpb.vouler.model.Usuario;

public class VouLerApplication extends Application implements TextToSpeech.OnInitListener{

    private TextToSpeech toSpeech;
    private Usuario usuario;

    private int quantLessions = 4;

    private List<Level> levels = null;

    private Level levelActive;

    @Override
    public void onCreate() {
        super.onCreate();
        createLevels();
        toSpeech = new TextToSpeech(getBaseContext(), this);
        usuario = new Usuario(0, "b1");
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public void voiceText(String text) {
    }

    public void addPontuacao(){
        usuario.setPontuacao(usuario.getPontuacao() + 20);
    }

    public int getPontuacao(){
        return usuario.getPontuacao();
    }

    public String getLevelUser(){
        return usuario.getKeyLevel();
    }

    @Override
    public void onInit(int i) {
        Locale locale = new Locale("pt-BR", "BR");
        toSpeech.setLanguage(locale);
    }

    public String capturarAudio(){
        return "";
    }

    public int getQuantLessions() {
        return quantLessions;
    }

    public void setQuantLessions(int quantLessions) {
        this.quantLessions = quantLessions;
    }

    public void decrementarLession(){
        this.quantLessions -= 1;
    }

    public void setLevelActive(Level levelActive) {
        this.levelActive = levelActive;
    }

    public Level getLevelActive() {
        return levelActive;
    }

    public Level searchLevelTag(String TAG){
        for(Level level : levels){
            if(level.getTAG().equalsIgnoreCase(TAG)){
                return level;
            }
        }

        return null;
    }

    public Exercice getExerciceRandom(int lession){
        Random random = new Random();
        int valor = random.nextInt(levelActive.getLessions()[lession].getExercices().size() - 1);
        return levelActive.getLessions()[lession].getExercices().get(valor);
    }

    public Exercice getExerciceTo(int lession, long id) {
        for(Exercice exercice : levelActive.getLessions()[lession].getExercices()){
            if(exercice.get_id() == id){
                return exercice;
            }
        }
        return null;
    }

    private void createLevels(){
        levels = Arrays.asList(

                new Level(0, "basic01", "Básico 01", 1, new Lession[]{
                        new Lession(0, "Vogais A, E e I", Arrays.asList(
                                new Exercice1(10, 1, "Qual destes é um elefante?",
                                        Arrays.asList(R.drawable.img_peixe, R.drawable.img_elefante, R.drawable.img_dog, R.drawable.img_cat),
                                        Arrays.asList("Peixe","Elefante","Cachorro","Gato"),
                                        "elefante"),
                                new Exercice2(11, 2, "Complete a palavra", "Cach__rr__",
                                        Arrays.asList("a", "e", "i", "o"),
                                        "o"),
                                new Exercice3(12, 3, "Qual o nome da imagem?", R.drawable.img_cat, "gato"),
                                new Exercice4(13, 4, "Qual o nome da imagem?", R.drawable.img_peixe, "peixe")
                        )),
                        new Lession(0, "Vogais O e U", Arrays.asList(
                                new Exercice1(10, 1, "Qual destes é um elefante?",
                                        Arrays.asList(R.drawable.img_peixe, R.drawable.img_elefante, R.drawable.img_dog, R.drawable.img_cat),
                                        Arrays.asList("Peixe","Elefante","Cachorro","Gato"),
                                        "elefante"),
                                new Exercice2(11, 2, "Complete a palavra", "Cach__rr__",
                                        Arrays.asList("a", "e", "i", "o"),
                                        "o"),
                                new Exercice3(12, 3, "Qual o nome da imagem?", R.drawable.img_cat, "gato"),
                                new Exercice4(13, 4, "Qual o nome da imagem?", R.drawable.img_peixe, "peixe")
                        ))
                }),
                new Level(0, "basic02", "Básico 02", 1, new Lession[]{
                        new Lession(0, "Palavras com A e E", Arrays.asList(
                                new Exercice1(10, 1, "Qual destes é um elefante?",
                                        Arrays.asList(R.drawable.img_peixe, R.drawable.img_elefante, R.drawable.img_dog, R.drawable.img_cat),
                                        Arrays.asList("Peixe","Elefante","Cachorro","Gato"),
                                        "elefante"),
                                new Exercice2(11, 2, "Complete a palavra", "Cach__rr__",
                                        Arrays.asList("a", "e", "i", "o"),
                                        "o"),
                                new Exercice3(12, 3, "Qual o nome da imagem?", R.drawable.img_cat, "gato"),
                                new Exercice4(13, 4, "Qual o nome da imagem?", R.drawable.img_peixe, "peixe")
                        )),
                        new Lession(0, "Palavras com I, O e U", Arrays.asList(
                                new Exercice1(10, 1, "Qual destes é um elefante?",
                                        Arrays.asList(R.drawable.img_peixe, R.drawable.img_elefante, R.drawable.img_dog, R.drawable.img_cat),
                                        Arrays.asList("Peixe","Elefante","Cachorro","Gato"),
                                        "elefante"),
                                new Exercice2(11, 2, "Complete a palavra", "Cach__rr__",
                                        Arrays.asList("a", "e", "i", "o"),
                                        "o"),
                                new Exercice3(12, 3, "Qual o nome da imagem?", R.drawable.img_cat, "gato"),
                                new Exercice4(13, 4, "Qual o nome da imagem?", R.drawable.img_peixe, "peixe")
                        ))
                }),
                new Level(0, "pratice01", "Praticar", 1, new Lession[]{
                        new Lession(0, "Vogais", Arrays.asList(
                                new Exercice1(10, 1, "Qual destes é um elefante?",
                                        Arrays.asList(R.drawable.img_peixe, R.drawable.img_elefante, R.drawable.img_dog, R.drawable.img_cat),
                                        Arrays.asList("Peixe","Elefante","Cachorro","Gato"),
                                        "elefante"),
                                new Exercice2(11, 2, "Complete a palavra", "Cach__rr__",
                                        Arrays.asList("a", "e", "i", "o"),
                                        "o"),
                                new Exercice3(12, 3, "Qual o nome da imagem?", R.drawable.img_cat, "gato"),
                                new Exercice4(13, 4, "Qual o nome da imagem?", R.drawable.img_peixe, "peixe")
                        )),
                        new Lession(0, "Palavras", Arrays.asList(
                                new Exercice1(10, 1, "Qual destes é um elefante?",
                                        Arrays.asList(R.drawable.img_peixe, R.drawable.img_elefante, R.drawable.img_dog, R.drawable.img_cat),
                                        Arrays.asList("Peixe","Elefante","Cachorro","Gato"),
                                        "Elefante"),
                                new Exercice2(11, 2, "Complete a palavra", "Cach__rr__",
                                        Arrays.asList("a", "e", "i", "o"),
                                        "o"),
                                new Exercice3(12, 3, "Qual o nome da imagem?", R.drawable.img_cat, "gato"),
                                new Exercice4(13, 4, "Qual o nome da imagem?", R.drawable.img_peixe, "peixe")
                        ))
                }),
                new Level(0, "consonants1", "Consoantes 01", 1, new Lession[]{
                        new Lession(0, "Consoantes B, C, D e E", Arrays.asList(
                                new Exercice1(10, 1, "Qual destes é um elefante?",
                                        Arrays.asList(R.drawable.img_peixe, R.drawable.img_elefante, R.drawable.img_dog, R.drawable.img_cat),
                                        Arrays.asList("Peixe","Elefante","Cachorro","Gato"),
                                        "elefante"),
                                new Exercice2(11, 2, "Complete a palavra", "Cach__rr__",
                                        Arrays.asList("a", "e", "i", "o"),
                                        "o"),
                                new Exercice3(12, 3, "Qual o nome da imagem?", R.drawable.img_cat, "gato"),
                                new Exercice4(13, 4, "Qual o nome da imagem?", R.drawable.img_peixe, "peixe")
                        )),
                        new Lession(0, "Consoantes E, F, G e H", Arrays.asList(
                                new Exercice1(10, 1, "Qual destes é um elefante?",
                                        Arrays.asList(R.drawable.img_peixe, R.drawable.img_elefante, R.drawable.img_dog, R.drawable.img_cat),
                                        Arrays.asList("Peixe","Elefante","Cachorro","Gato"),
                                        "elefante"),
                                new Exercice2(11, 2, "Complete a palavra", "Cach__rr__",
                                        Arrays.asList("a", "e", "i", "o"),
                                        "o"),
                                new Exercice3(12, 3, "Qual o nome da imagem?", R.drawable.img_cat, "gato"),
                                new Exercice4(13, 4, "Qual o nome da imagem?", R.drawable.img_peixe, "peixe")
                        ))
                })
        );
    }
}
