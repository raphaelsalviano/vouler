package br.com.ufpb.vouler.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import br.com.ufpb.vouler.R;
import br.com.ufpb.vouler.VouLerApplication;
import br.com.ufpb.vouler.activities.LevelActivity;
import br.com.ufpb.vouler.model.Exercice;
import br.com.ufpb.vouler.model.Exercice2;

public class FragmentExerc2 extends Fragment {

    private VouLerApplication application;

    private boolean isContinue = false;

    private TextView mTitle;
    private View mButtonAudio;

    private TextView mWord;

    private RadioButton button1;
    private RadioButton button2;
    private RadioButton button3;
    private RadioButton button4;

    private Button mButtonConfirm;

    private Exercice2 exercice2;
    private int lession;
    private String TAG;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        application = (VouLerApplication) getActivity().getApplicationContext();
        if(getArguments() != null){
            lession = getArguments().getInt("lession");
            long id = getArguments().getLong("id");
            TAG = getArguments().getString("TAG");
            exercice2 = (Exercice2) application.getExerciceTo(lession, id);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_exerc2, container, false);

        findViewById(view);
        loadingQuestion(exercice2);
        onCLickButtonSend(onCLick());

        return view;
    }

    private void findViewById(View view){
        mTitle = (TextView) view.findViewById(R.id.title_exerc2);
        mButtonAudio = view.findViewById(R.id.audio_exerc2);

        mWord = (TextView) view.findViewById(R.id.textCompleteexerc2);

        button1 = (RadioButton) view.findViewById(R.id.radioButton);
        button2 = (RadioButton) view.findViewById(R.id.radioButton2);
        button3 = (RadioButton) view.findViewById(R.id.radioButton3);
        button4 = (RadioButton) view.findViewById(R.id.radioButton4);

        mButtonConfirm = (Button) view.findViewById(R.id.button_send_confirmed2);
    }

    private void loadingQuestion(final Exercice2 exercice){
        mTitle.setText(exercice.getTitle());
        mButtonAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                application.voiceText(exercice.getTitle());
            }
        });

        mWord.setText(exercice.getWord());

        button1.setText(exercice.getAlternatives().get(0));
        button2.setText(exercice.getAlternatives().get(1));
        button3.setText(exercice.getAlternatives().get(2));
        button4.setText(exercice.getAlternatives().get(3));

    }

    private String[] onCLick(){
        final String[] selecion = {""};

        button1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    selecion[0] = button1.getText().toString();
                    button2.setChecked(false);
                    button3.setChecked(false);
                    button4.setChecked(false);
                    mButtonConfirm.setEnabled(true);
                    mButtonConfirm.setBackgroundResource(R.drawable.raised_button_blue);
                }else{
                    mButtonConfirm.setEnabled(false);
                    mButtonConfirm.setBackgroundResource(R.drawable.btn_primary_disable);
                }
            }
        });

        button2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    button1.setChecked(false);
                    button3.setChecked(false);
                    button4.setChecked(false);
                    selecion[0] = button2.getText().toString();
                    mButtonConfirm.setEnabled(true);
                    mButtonConfirm.setBackgroundResource(R.drawable.raised_button_blue);
                }else{
                    mButtonConfirm.setEnabled(false);
                    mButtonConfirm.setBackgroundResource(R.drawable.btn_primary_disable);
                }
            }
        });

        button3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    button1.setChecked(false);
                    button2.setChecked(false);
                    button4.setChecked(false);
                    selecion[0] = button3.getText().toString();
                    mButtonConfirm.setEnabled(true);
                    mButtonConfirm.setBackgroundResource(R.drawable.raised_button_blue);
                }else{
                    mButtonConfirm.setEnabled(false);
                    mButtonConfirm.setBackgroundResource(R.drawable.btn_primary_disable);
                }
            }
        });

        button4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    button2.setChecked(false);
                    button3.setChecked(false);
                    button1.setChecked(false);
                    selecion[0] = button4.getText().toString();
                    mButtonConfirm.setEnabled(true);
                    mButtonConfirm.setBackgroundResource(R.drawable.raised_button_blue);
                }else{
                    mButtonConfirm.setEnabled(false);
                    mButtonConfirm.setBackgroundResource(R.drawable.btn_primary_disable);
                }
            }
        });

        return selecion;
    }

    private void onCLickButtonSend(final String[] selecion){
        mButtonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isContinue){
                    if(selecion[0].equalsIgnoreCase(exercice2.getResponse())){
                        Snackbar.make(view, "Correto!", Snackbar.LENGTH_SHORT).show();
                    }else{
                        Snackbar.make(view, "Errado!", Snackbar.LENGTH_SHORT).show();
                    }
                    mButtonConfirm.setText("Continuar");
                    mButtonConfirm.setAllCaps(true);
                    isContinue = true;
                }else{
                    // Muda pro proximo exercicio
                    application.decrementarLession();
                    trocarTela();
                }
            }
        });
    }

    private void trocarTela(){
        Fragment fragment = null;
        Exercice exercice = application.getExerciceRandom(lession);
        if(exercice.getTypeExercice() == 1){
            fragment = new FragmentExerc1();
        }else if(exercice.getTypeExercice() == 2){
            fragment = new FragmentExerc2();
        }else if(exercice.getTypeExercice() == 3){
            fragment = new FragmentExerc3();
        }else{
            fragment = new FragmentExerc4();
        }

        Bundle bundle = new Bundle();
        bundle.putInt("lession", lession);
        bundle.putLong("id", exercice.get_id());
        fragment.setArguments(bundle);

        Log.i("vo", "" + application.getQuantLessions());

        if(application.getQuantLessions() > 0){
            getFragmentManager().beginTransaction().replace(R.id.container_exercices, fragment).commit();
            onDestroy();
        }else{
            Intent intent = new Intent(getActivity(), LevelActivity.class);
            intent.putExtra("TAG", TAG);
            startActivity(intent);
            getActivity().finish();
        }
    }

}
