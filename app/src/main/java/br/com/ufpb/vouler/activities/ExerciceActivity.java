package br.com.ufpb.vouler.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import br.com.ufpb.vouler.R;
import br.com.ufpb.vouler.VouLerApplication;
import br.com.ufpb.vouler.fragment.FragmentExerc1;
import br.com.ufpb.vouler.fragment.FragmentExerc2;
import br.com.ufpb.vouler.fragment.FragmentExerc3;
import br.com.ufpb.vouler.fragment.FragmentExerc4;
import br.com.ufpb.vouler.model.Exercice;

public class ExerciceActivity extends AppCompatActivity {

    private VouLerApplication application;

    private int lession = -1;
    private String TAG = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercice);

        application = (VouLerApplication) getApplicationContext();
        if(getIntent().getExtras() != null){
            TAG = getIntent().getExtras().getString("TAG");
        }

        if(getIntent().getExtras() != null){
            lession = getIntent().getExtras().getInt("lession");
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_exercice1);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Exercice exercice = application.getExerciceRandom(lession);
        Fragment fragment = null;

        if(exercice.getTypeExercice() == 1){
            fragment = new FragmentExerc1();
        }else if(exercice.getTypeExercice() == 2){
            fragment = new FragmentExerc2();
        }else if(exercice.getTypeExercice() == 3){
            fragment = new FragmentExerc3();
        }else{
            fragment = new FragmentExerc4();
        }

        Bundle bundle = new Bundle();
        bundle.putInt("lession", lession);
        bundle.putString("TAG", TAG);
        bundle.putLong("id", exercice.get_id());
        fragment.setArguments(bundle);

        getSupportFragmentManager().beginTransaction().replace(R.id.container_exercices, fragment).commit();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_exercice, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if(id == android.R.id.home){
            new AlertDialog.Builder(this)
                    .setTitle("Tem certeza que quer sair?")
                    .setMessage("Todo o progresso nesta sessão será perdido.")
                    .setPositiveButton("DESISTIR", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent intent = new Intent(ExerciceActivity.this, LevelActivity.class);
                            application.setQuantLessions(4);
                            startActivity(intent);
                            finish();
                        }
                    })
                    .setNegativeButton("Cancelar", null)
                    .create().show();
        }else if(id == R.id.points_exer1_action){
            Toast.makeText(ExerciceActivity.this, "Sua pontuação é de " + application.getPontuacao() + " pontos", Toast.LENGTH_SHORT).show();
        }

        return super.onOptionsItemSelected(item);
    }
}
