package br.com.ufpb.vouler.model;

/**
 * Created by rapha on 07/06/2016.
 */
public interface Exercice {

    int getTypeExercice();

    long get_id();
}
