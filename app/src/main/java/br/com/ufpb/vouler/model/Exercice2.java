package br.com.ufpb.vouler.model;

import java.util.List;

/**
 * Created by rapha on 07/06/2016.
 */
public class Exercice2 implements Exercice {

    private long _id;
    private int typeExercice;
    private String title;
    private String word;
    private List<String> alternatives;
    private String response;

    public Exercice2(long _id, int typeExercice, String title, String word, List<String> alternatives, String response) {
        this._id = _id;
        this.typeExercice = typeExercice;
        this.title = title;
        this.word = word;
        this.alternatives = alternatives;
        this.response = response;
    }

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public int getTypeExercice() {
        return typeExercice;
    }

    public void setTypeExercice(int typeExercice) {
        this.typeExercice = typeExercice;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getAlternatives() {
        return alternatives;
    }

    public void setAlternatives(List<String> alternatives) {
        this.alternatives = alternatives;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }
}
