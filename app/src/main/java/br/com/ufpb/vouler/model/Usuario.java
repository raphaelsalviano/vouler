package br.com.ufpb.vouler.model;

/**
 * Created by rapha on 09/06/2016.
 */
public class Usuario {

    private int pontuacao;
    private String keyLevel;

    public Usuario(int pontuacao, String keyLevel) {
        this.pontuacao = pontuacao;
        this.keyLevel = keyLevel;
    }

    public int getPontuacao() {
        return pontuacao;
    }

    public void setPontuacao(int pontuacao) {
        this.pontuacao = pontuacao;
    }

    public String getKeyLevel() {
        return keyLevel;
    }

    public void setKeyLevel(String keyLevel) {
        this.keyLevel = keyLevel;
    }
}
