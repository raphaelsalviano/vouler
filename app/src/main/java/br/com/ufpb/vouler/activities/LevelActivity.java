package br.com.ufpb.vouler.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import br.com.ufpb.vouler.R;
import br.com.ufpb.vouler.VouLerApplication;
import br.com.ufpb.vouler.model.Level;

public class LevelActivity extends AppCompatActivity {

    private VouLerApplication application;

    private TextView nameLevel;

    private TextView mTitle1;
    private TextView mSubtitle1;
    private Button mButton1;

    private TextView mTitle2;
    private TextView mSubtitle2;
    private Button mButton2;

    private String TAG;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level);

        application = (VouLerApplication) getApplicationContext();

        TAG = "";
        if(getIntent().getExtras() != null){
            TAG = getIntent().getExtras().getString("TAG");
        }

        Level level = application.searchLevelTag(TAG);
        if(level == null){
            level = application.getLevelActive();
        }
        application.setLevelActive(level);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_level);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        nameLevel = (TextView) findViewById(R.id.name_level);
        nameLevel.setText(level.getNameLevel());

        mSubtitle1 = (TextView) findViewById(R.id.text_description1);
        mSubtitle1.setText(level.getLessions()[0].getDescription());

        mButton1 = (Button) findViewById(R.id.button_start_level1);
        mButton1.setOnClickListener(new OnCLick(true));

        mSubtitle2 = (TextView) findViewById(R.id.text_description2);
        mSubtitle2.setText(level.getLessions()[1].getDescription());

        mButton2 = (Button) findViewById(R.id.button_start_level2);
        mButton2.setOnClickListener(new OnCLick(false));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if(id == android.R.id.home){
            Intent intent = new Intent(this, MainActivity.class);
            application.setLevelActive(null);
            intent.putExtra("TAG", TAG);
            startActivity(intent);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }


    private class OnCLick implements View.OnClickListener{

        private boolean button1 = false;

        public OnCLick(boolean button1) {
            this.button1 = button1;
        }

        @Override
        public void onClick(View view) {
            application.setQuantLessions(4);
            if(button1){
                Intent intent = new Intent(LevelActivity.this, ExerciceActivity.class);
                intent.putExtra("lession", 0);
                intent.putExtra("TAG", TAG);
                startActivity(intent);
                finish();
            }else{
                Intent intent = new Intent(LevelActivity.this, ExerciceActivity.class);
                intent.putExtra("lession", 1);
                intent.putExtra("TAG", TAG);
                startActivity(intent);
                finish();
            }
        }
    }
}
